INSERT INTO transactionsByBrokerReport (brokerName, transactionType, total, avgSale)
SELECT
  BROKERNAME
  , TRANSACTIONTYPE
  , SUM(PRICE * QUANTITY) AS TOTAL
  , AVG(PRICE * QUANTITY) as AVGSALE
  FROM transactions
  GROUP BY BROKERNAME, TRANSACTIONTYPE

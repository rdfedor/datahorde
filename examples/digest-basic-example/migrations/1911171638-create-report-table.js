module.exports = {
  up: (queryInterface, Sequelize, data) => queryInterface.createTable('transactionsByBrokerReport', {
    brokerName: Sequelize.STRING,
    transactionType: Sequelize.STRING,
    total: Sequelize.INTEGER,
    avgSale: Sequelize.INTEGER
  }),
  down: (queryInterface, Sequelize, data) => queryInterface.dropTable('transactionsByBrokerReport')
}

INSERT INTO movies (rank, release, release_studio, revenue, theaters)
SELECT
  rank
  , release
  , release_studio
  , REPLACE(REPLACE(revenue, '$', ''), ',', '')
  , REPLACE(theaters, ',', '')
FROM raw_movie_data
WHERE lower(rank) != 'rank';

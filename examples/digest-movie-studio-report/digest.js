module.exports = ({ year, week }) => {
  const defaultYear = 2019
  const defaultWeek = 42
  return {
    crawler: [
      {
        paths: `https://www.boxofficemojo.com/weekend/${year ||
        defaultYear}W${week || defaultWeek}/`,
        output: './output/movie-revenue-listing.csv',
        headers: ['rank', 'release', 'release_studio', 'revenue', 'theaters'],
        rowSelector: 'tr',
        browser: true,
        columnSelector: [
          '.mojo-field-type-rank',
          '.mojo-field-type-release',
          '.mojo-field-type-release_studios',
          'td:nth-child(5):text',
          'td:nth-child(7):text'
        ]
      }
    ],
    devour: [
      {
        path: './output/movie-revenue-listing.csv',
        schema: {
          rank: 'int',
          release: 'str(45)',
          release_studio: 'str(45)',
          revenue: 'str(25)',
          theaters: 'int'
        },
        tableName: 'raw_movie_data'
      }
    ],
    paths: ['sql/import-data.ejs.sql'],
    defaultData: {
      year: defaultYear,
      weeK: defaultWeek
    },
    egressions: [
      {
        outputPath: 'output/movie_performance_by_studio_report.csv',
        path: 'reports/movie_performance_by_studio.sql'
      }
    ]
  }
}

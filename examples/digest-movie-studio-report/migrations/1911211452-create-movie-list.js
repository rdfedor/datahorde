module.exports = {
  up: (queryInterface, Sequelize, data) => {
    console.log(Sequelize.NOW)
    return queryInterface.createTable('movies', {
      year: { type: Sequelize.INTEGER, defaultValue: data.year },
      week: { type: Sequelize.INTEGER, defaultValue: data.week },
      rank: Sequelize.INTEGER,
      release: Sequelize.STRING,
      release_studio: Sequelize.STRING,
      revenue: Sequelize.INTEGER,
      theaters: Sequelize.INTEGER,
      created_on: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    })
  },
  down: (queryInterface, Sequelize, data) => queryInterface.dropTable('movies')
}

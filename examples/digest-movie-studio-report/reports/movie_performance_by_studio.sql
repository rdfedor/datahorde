SELECT
  year,
  week,
  release_studio,
  count(*) as total_movies,
  SUM(revenue) AS total_revenue,
  SUM(theaters) AS total_theaters,
  CAST(AVG(revenue) AS INT) AS avg_revenue,
  CAST(AVG(theaters) AS INT) AS avg_theaters,
  created_on
FROM movies
GROUP BY
  release_studio,
  year,
  week,
  created_on
ORDER BY SUM(revenue) desc

module.exports = {
  DEFAULT_PROFILE: 'generic',
  DEFAULT_ROW_SELECTOR: 'body',
  DEFAULT_COLUMN_SELECTOR: 'a',
  DEFAULT_THREADS: 1,
  DEFAULT_DELIMETER: '|',
  DEFAULT_HEADERS: true,
  DEFAULT_RENAME_HEADERS: false,
  DEFAULT_QUEUE_URL_KEY: 'url',
  ON_DATA: 'DATA',
  ON_FINISHED: 'FINISHED',
  ON_URL_DISCOVERED: 'URL_DISCOVERED',
  ON_ERROR: 'ERROR'
}

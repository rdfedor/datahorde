const debug = require('debug')('DataHorde:CrawlerGenericProfile')
const axios = require('axios')
const SocksProxyAgent = require('socks-proxy-agent')

const EventEmitter = require('events')

const GenericHtmlParser = require('./html-parser')
const {
  ON_FINISHED, ON_DATA, ON_URL_DISCOVERED, ON_ERROR
} = require('../../constants')

/**
 * Axios based crawler profile
 * @extends {EventEmitter}
 */
class CrawlerGenericProfile extends EventEmitter {
  /**
   * Creates an instance of CrawlerGenericProfile.
   * @param {String} url Web address to start crawling
   * @param {Object} options Options to use with Axios
   * @param {String} proxy Socks proxy to use with requests
   * @memberof CrawlerGenericProfile
   */
  constructor (url, options, proxy) {
    debug(
      `Initializing Generic Profile Crawler Constructor, ${JSON.stringify({
        url,
        options
      })}`
    )

    super()

    this.validate(url && url.length, 'url defined')
    this.validate(options, 'options defined')
    this.validate(
      options.columnSelector && options.columnSelector.length,
      'columnSelector defined'
    )
    this.validate(
      options.rowSelector && options.rowSelector.length,
      'rowSelector defined'
    )

    this.url = url
    this.options = options
    this.proxy = proxy
  }

  /**
   * Base function for validating conditions
   *
   * @param {Boolean} condition
   * @param {String} message
   * @memberof CrawlerGenericProfile
   */
  validate (condition, message) {
    if (!condition) {
      throw new Error(`Failed Validation Check - ${message}`, {
        url: this.url,
        options: this.options
      })
    }
  }

  /**
   * Starts the crawler
   *
   * @memberof CrawlerGenericProfile
   */
  start () {
    debug(`Making request to ${this.url}`)

    const request = {
      method: 'GET',
      url: this.url,
      headers: this.options.headers
    }

    if (this.proxy) {
      debug(`Using proxy ${this.proxy}`)
      // create the socksAgent for axios
      request.httpsAgent = new SocksProxyAgent(this.proxy)
    }

    axios(request)
      .then((response) => {
        const {
          baseUrl, rowSelector, columnSelector, nextPageLinkSelector
        } = {
          baseUrl: this.url,
          rowSelector: this.options.rowSelector,
          columnSelector: this.options.columnSelector,
          nextPageLinkSelector: this.options.nextPageLinkSelector
        }
        const parser = new GenericHtmlParser(
          rowSelector,
          columnSelector,
          baseUrl,
          nextPageLinkSelector
        )
        const doc = parser.parseDocument(response.data)

        // Emit collected data
        this.emit(ON_DATA, doc.rows)

        // Resolve the nextPageLink if found and emit
        if (doc.nextPageLink) {
          this.emit(ON_URL_DISCOVERED, doc.nextPageLink)
        }
      })
      .catch((err) => {
        const newError = err
        newError.config = JSON.stringify({
          url: this.url,
          options: this.options
        })
        this.emit(ON_ERROR, newError)
      })
      .finally(() => {
        this.emit(ON_FINISHED)
      })
  }
}

module.exports = CrawlerGenericProfile

const debug = require('debug')('DataHorde:CrawlerGenericHtmlParser')
const url = require('url')
const cheerio = require('cheerio')

/**
 * Cheerios based html parse for generic profile
 */
class CrawlerGenericHtmlParser {
  /**
   *Creates an instance of CrawlerGenericHtmlParser.
   * @param {String} rowSelector Css selector for row
   * @param {String} columnSelector Css selector for column
   * @param {String} [baseUrl=null] Path to base of url
   * @param {String} [nextPageLinkSelector=null] Css selector for next page link
   * @memberof CrawlerGenericHtmlParser
   */
  constructor (
    rowSelector,
    columnSelector,
    baseUrl = null,
    nextPageLinkSelector = null
  ) {
    this.baseUrl = baseUrl
    this.rowSelector = rowSelector
    this.columnSelector = columnSelector
    this.nextPageLinkSelector = nextPageLinkSelector
  }

  /**
   * Retrieves value from selector in cheerio document
   *
   * @param {Cheerio} $ Cheerio wrapped element
   * @param {String} actionSelector Css selector to select value from
   * @returns {String} Value from selector call
   * @memberof CrawlerGenericHtmlParser
   */
  parseSelector ($, actionSelector) {
    const splitActionSelector = actionSelector.split(':')
    let selectorAction = 'text'
    let selector = actionSelector
    let actionParameter

    if (splitActionSelector.length > 1) {
      selectorAction = splitActionSelector.pop()
      selector = splitActionSelector.join(':')

      if (selectorAction.indexOf('(') > -1) {
        const startInterval = selectorAction.indexOf('(')
        const parameterLength = selectorAction.length - startInterval - 2
        actionParameter = selectorAction.substr(
          startInterval + 1,
          parameterLength
        )
        selectorAction = selectorAction.substr(0, startInterval)
      }
    }

    debug(
      `Finding ${selector} and performing ${selectorAction}(${actionParameter})`
    )
    const selectorEl = $.find(selector)

    if (!selectorEl[selectorAction]) {
      throw new Error(
        `${selectorAction} is an unknown column selector function`
      )
    }

    let actionValue = selectorEl[selectorAction](actionParameter)

    if (actionValue) {
      actionValue = actionValue.trim()

      if (actionParameter && actionParameter === 'href' && this.baseUrl) {
        actionValue = (new url.URL(actionValue, this.baseUrl)).href
      }
    }

    debug(`Found ${actionValue}`)

    return actionValue
  }

  /**
   * Parse given string document and return selector values
   *
   * @param {String} document Html Document
   * @returns {Object} {nextPageLink, rows} Object with the next page to scrape and rows from the current
   * @memberof CrawlerCrawlerGenericHtmlParser
   */
  parseDocument (document) {
    debug('Parsing document with the following values', {
      rowSelector: this.rowSelector,
      columnSelector: this.columnSelector,
      baseUrl: this.baseUrl
    })
    const $ = cheerio.load(document)
    const rows = $(this.rowSelector)
    const mappedRows = []
    let nextPageLink

    debug(
      `Found ${rows.length} rows${
        rows.length ? ', searching for selectors' : ''
      }`
    )

    rows.each((index, row) => {
      mappedRows.push(
        this.columnSelector.map((selector) => this.parseSelector($(row), selector))
      )
    })

    if (this.nextPageLinkSelector) {
      nextPageLink = this.parseSelector($('body'), this.nextPageLinkSelector)
    }

    return {
      nextPageLink,
      rows: mappedRows
    }
  }
}

module.exports = CrawlerGenericHtmlParser

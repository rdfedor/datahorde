const debug = require('debug')('DataHorde:Crawler')
const EventEmitter = require('events')
const csv = require('fast-csv')
const fs = require('fs')
const path = require('path')
const stringify = require('json-stringify-safe')

const {
  DEFAULT_PROFILE,
  DEFAULT_DELIMETER,
  DEFAULT_HEADERS,
  DEFAULT_RENAME_HEADERS,
  DEFAULT_QUEUE_URL_KEY,
  ON_DATA,
  ON_FINISHED,
  ON_URL_DISCOVERED,
  ON_ERROR
} = require('./constants')
const profileMap = require('./profile-map')

/**
 * Crawler service for tracking and processing url queue
 * @extends {EventEmitter}
 */
class Crawler extends EventEmitter {
  /**
   * Retrieve a list of crawler profiles
   *
   * @static
   * @returns <Array> list of crawler profiles
   */
  static getProfiles () {
    return Object.keys(profileMap)
  }

  /**
   *Creates an instance of Crawler.
   * @param {String} rowSelector Css selector to set row data
   * @param {String} columnSelector Css Selector to select column data
   * @param {Array} [headers={}] Headers to be sent with request
   * @param {String} [nextPageLinkSelector=undefined] Css selector where next page link can be found
   * @param {String} [proxy=undefined] Proxy to pass requests through
   * @param {number} [threads=1] Number of simultaneous processes
   * @memberof Crawler
   */
  constructor (
    rowSelector,
    columnSelector,
    headers = {},
    nextPageLinkSelector = undefined,
    proxy = undefined,
    threads = 1
  ) {
    debug(
      `Crawler created with ${stringify({
        rowSelector,
        columnSelector,
        headers,
        nextPageLinkSelector,
        proxy,
        threads
      })}`
    )

    super()

    this.urlQueue = []
    this.rowSelector = rowSelector
    this.columnSelector = columnSelector
    this.headers = headers
    this.nextPageLinkSelector = nextPageLinkSelector
    this.profile = DEFAULT_PROFILE
    this.proxy = proxy
    this.running = 0
    this.threads = threads
  }

  /**
   * Set the number of threads to run at the same time
   *
   * @param {Number} count
   * @memberof Crawler
   */
  setThreads (count) {
    this.threads = count
  }

  /**
   *  Adds url(s) to queue
   *
   * @param {Array|String} url Url or Array of urls to add to queue
   * @returns {this}
   */
  queueUrl (url) {
    if (url) {
      if (Array.isArray(url)) {
        debug(`Adding url to queue(${this.urlQueue.length}), ${url.join(',')}`)
        this.urlQueue = this.urlQueue.concat(url)
      } else {
        debug(`Adding url to queue(${this.urlQueue.length}), ${url}`)
        this.urlQueue.push(url)
      }
    }

    return this
  }

  /**
   * Queues the urls from a given csv
   *
   * @param {String} inputFile Path to CSV to import
   * @param {Boolean|Array} [headers=DEFAULT_HEADERS] True if the csv has headers or array of headers
   * @param {String} [delimiter=DEFAULT_DELIMETER] Column separater from CSV
   * @param {String} [queueUrlKey=DEFAULT_QUEUE_URL_KEY] Key to use to queue url
   * @param {Boolean} [renameHeaders=DEFAULT_RENAME_HEADERS] Re-maps columns to headers
   * @returns {Promise}
   * @memberof Crawler
   */
  queueUrlFromCsv (
    inputFile,
    headers = DEFAULT_HEADERS,
    delimiter = DEFAULT_DELIMETER,
    queueUrlKey = DEFAULT_QUEUE_URL_KEY,
    renameHeaders = DEFAULT_RENAME_HEADERS
  ) {
    let urlCount = 0

    return new Promise((resolve, reject) => {
      debug('Attempting to load csv, ', {
        inputFile,
        options: {
          headers,
          delimiter,
          renameHeaders
        }
      })
      csv
        .parseFile(inputFile, {
          headers,
          delimiter,
          renameHeaders
        })
        .on('data-invalid', (row, rowNumber) => console.log(
          `Invalid [rowNumber=${rowNumber}] [row=${JSON.stringify(row)}]`
        ))
        .on('error', (error) => reject(error))
        .on('data', (row) => {
          urlCount += 1
          this.queueUrl(row[queueUrlKey] || false)
        })
        .on('end', () => {
          debug(`Loaded ${urlCount} urls from ${inputFile}`)
          resolve(urlCount)
        })
    })
  }

  /**
   * Sets the crawler profile
   *
   * @param {string} profile Name of the profile to crawl with
   * @returns {this}
   */
  setProfile (profile) {
    if (!profileMap[profile]) {
      throw new Error('Profile not supported')
    }

    debug(`Setting profile to ${profile}`)

    this.profile = profile

    return this
  }

  /**
   * Returns the current profile
   *
   * @returns {String} Current profile
   */
  getProfile () {
    return this.profile
  }

  /**
   * Returns the next instance configuration with the next url
   *
   * @returns {Object}
   * @memberof Crawler
   */
  nextInstanceConfiguration () {
    const url = this.urlQueue.pop()

    return {
      proxy: this.proxy,
      url,
      options: {
        rowSelector: this.rowSelector,
        columnSelector: this.columnSelector,
        headers: this.headers,
        nextPageLinkSelector: this.nextPageLinkSelector
      }
    }
  }

  /**
   * Status of the url queue
   *
   * @returns {Boolean}
   */
  hasUrlQueue () {
    return this.urlQueue.length > 0
  }

  /**
   * Enables browser emulation setting the header of the request
   *
   * @memberof Crawler
   */
  toggleBrowserEmulation () {
    this.headers = this.headers || {}
    this.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
  }

  /**
   * A recursive function for crawling the url queue until it's empty
   */
  startNextCrawler () {
    if (this.hasUrlQueue()) {
      const ProfileClass = profileMap[this.profile]
      const { url, proxy, options } = this.nextInstanceConfiguration()

      debug(
        `Creating new crawler instance ${stringify({ url, proxy, options })}`
      )
      const crawler = new ProfileClass(url, options, proxy)

      crawler.on(ON_ERROR, (err) => console.error(err))
      crawler.on(ON_DATA, (row) => {
        this.emit(
          ON_DATA,
          row.filter((row) => row.join('').length > 0)
        )
      })
      crawler.on(ON_URL_DISCOVERED, (url) => this.urlQueue.push(url))
      crawler.on(ON_FINISHED, () => {
        this.running--
        this.startNextCrawler()
      })

      this.running++
      crawler.start()

      if (this.running < this.threads) {
        this.startNextCrawler()
      }
    } else if (!this.running) {
      this.emit(ON_FINISHED)
    }
  }

  /**
   * Writes results of the scrape to csv
   *
   * @param {String} outputFile Output path to write results to
   * @param {*} headers
   * @returns {Promise}
   * @memberof Crawler
   */
  outputToCsv (outputFile, headers, delimiter = '|') {
    return new Promise((resolve, reject) => {
      debug(`Outputing contents of export to ${outputFile}`)

      if (!fs.existsSync(path.dirname(outputFile))) {
        fs.mkdirSync(path.dirname(outputFile))
      }

      const outputHandler = fs.createWriteStream(outputFile)

      const csvStream = csv.format({
        delimiter,
        headers
      })

      csvStream.pipe(outputHandler)

      const onData = (rows) => {
        rows.forEach((row) => {
          debug(JSON.stringify(row))
          csvStream.write(row)
        })
      }

      const onFinished = () => {
        csvStream.end()
        this.removeListener(ON_FINISHED, onFinished)
        this.removeListener(ON_DATA, onData)
        resolve()
      }

      this.on(ON_DATA, onData)

      this.on(ON_FINISHED, onFinished)

      this.begin()
    })
  }

  /**
   * Entry point of the crawler which starts the process
   */
  begin () {
    debug(
      `Crawler started. Searching through ${this.urlQueue.length} urls using ${this.profile} profile.`
    )

    this.startNextCrawler()
  }
}

module.exports = Crawler

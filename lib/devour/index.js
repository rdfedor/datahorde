const debug = require('debug')('DataHorde:Devour')
const EventEmitter = require('events')
const fs = require('fs')
const path = require('path')
const csv = require('fast-csv')

const CommonDatabase = require('../common/database')
const DatabaseBufferedImport = require('./database/buffered-import')
const DatabaseModeler = require('./database/modeler')

const {
  DEFAULT_DATABASE_URI,
  DEFAULT_TABLE_NAME,
  DEFAULT_DELIMITER,
  DEFAULT_HAS_HEADERS,
  DEFAULT_RENAME_HEADERS,
  ON_FINISHED,
  ON_CREATED
} = require('./constants')

/**
 * Allows for the ingestion of file based csv
 *
 * @extends {EventEmitter}
 */
class Devour extends EventEmitter {
  /**
   * Returns an array of supported schema column types
   *
   * @static
   * @returns {Array} List of schema column types
   * @memberof Devour
   */
  static getSchemaColumnTypes () {
    return Array.from(DatabaseModeler.getColumnTypeMap().keys())
  }

  /**
   * Creates an instance of Devour.
   *
   * @param {String|CommonDatabase} [databaseUri=DEFAULT_DATABASE_URI] Uri path to database supported by Sequelize
   * @memberof Devour
   */
  constructor (databaseUri = DEFAULT_DATABASE_URI) {
    super()

    let database

    if (databaseUri instanceof CommonDatabase) {
      database = databaseUri
    } else {
      database = new CommonDatabase(databaseUri)
    }

    this.database = database
    this.databaseUri = databaseUri
  }

  /**
   * Initializes modeler schema in database
   *
   * @param {Object} schema Object describing the schema describing the table to import to
   * @param {String} [tableName=DEFAULT_TABLE_NAME] Name of the table to import data into
   * @param {boolean} [forceSync=false] Drops the table, if it exists, and re-creates
   * @returns {Promise}
   * @memberof Devour
   */
  initializeDatabaseImportQueue (
    schema,
    tableName = DEFAULT_TABLE_NAME,
    forceSync = false
  ) {
    return this.database
      .registerExtension(DatabaseBufferedImport, schema, tableName, forceSync)
      .then((queue) => {
        queue.on(ON_CREATED, (count) => this.emit(ON_CREATED, count))

        this.schema = schema
        this.queue = queue

        debug('Intialized import queue')
      })
  }

  /**
   * Removes the buffered-import extension
   *
   * @returns {Boolean}
   * @memberof Devour
   */
  unRegisterDatabaseImportQueue () {
    return this.database.unRegisterExtension(DatabaseBufferedImport)
  }

  /**
   * Imports a csv from file and queue the record for import
   *
   * @param {String} file Path to file to import
   * @param {Boolean} [hasHeaders=DEFAULT_HAS_HEADERS] whether or the csv has headers already
   * @param {String} [delimiter=DEFAULT_DELIMITER] Column separator in csv
   * @param {Boolean} [renameHeaders=DEFAULT_RENAME_HEADERS] if the headers don't match, rename them to match
   * @returns {Promise}
   * @memberof Devour
   */
  loadFromCsvFile (
    file,
    hasHeaders = DEFAULT_HAS_HEADERS,
    delimiter = DEFAULT_DELIMITER,
    renameHeaders = DEFAULT_RENAME_HEADERS
  ) {
    return new Promise((resolve, reject) => {
      let headers = true

      if (!hasHeaders || renameHeaders) {
        debug('Updating headers', { hasHeaders, renameHeaders })
        headers = Object.keys(this.schema)
      }

      debug(`Loading from csv ${file} with delimiter '${delimiter}'`, {
        hasHeaders,
        renameHeaders,
        headers
      })

      let count = 0

      const processOnFinished = () => {
        this.queue.removeListener(ON_FINISHED, processOnFinished)
        resolve(count)
      }

      this.queue.on(ON_FINISHED, processOnFinished)

      csv
        .parseFile(file, { delimiter, headers, renameHeaders })
        .on('data', (row) => {
          count += 1
          this.queue.pushRecord(row)
        })
        .on('error', (err) => reject(err))
        .on('end', () => {
          debug('Loading from csv finished.')
        })
    })
  }

  /**
   * Writes query results to csv
   *
   * @param {String} outputFile path to output the data to
   * @param {String} query query to run for the data to populate the csv
   * @param {String} [delimiter=DEFAULT_DELIMITER] csv column separator
   * @param {Boolean|Array} [headers=DEFAULT_HAS_HEADERS] If true, output the headers, if an array rename the headers
   * @returns {Promise}
   * @memberof Devour
   */
  egressToCsv (
    outputFile,
    query,
    delimiter = DEFAULT_DELIMITER,
    headers = DEFAULT_HAS_HEADERS
  ) {
    debug('Exporting data from database', {
      outputFile,
      query,
      delimiter,
      headers
    })
    return new Promise((resolve, reject) => {
      const csvStream = csv.format({
        delimiter,
        headers
      })

      if (!fs.existsSync(path.dirname(outputFile))) {
        fs.mkdirSync(path.dirname(outputFile))
      }

      const fsHandler = fs.createWriteStream(outputFile)

      csvStream.pipe(fsHandler)

      this.database
        .query(query, { raw: true })
        .then(([results, metadata]) => {
          results.forEach((row) => {
            debug(JSON.stringify(row))
            csvStream.write(row)
          })
          csvStream.on('end', () => {
            fsHandler.close()
            resolve(results.length)
          })

          csvStream.end()
        })
        .catch((err) => reject(err))
    })
  }

  /**
   * Closes remaining connections/handles
   *
   * @memberof Devour
   */
  close () {
    this.database.close()
  }
}

module.exports = Devour

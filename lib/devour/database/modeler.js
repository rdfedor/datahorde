const debug = require('debug')('DataHorde:DevourDatabaseModeler')

const { Model, DataTypes } = require('sequelize')
const { Map } = require('immutable')

const { DEFAULT_TABLE_NAME } = require('../constants')

/**
 * Model builder for sequelize
 */
class DevourDatabaseModeler {
  /**
   *  Returns a user map of database column types
   *
   * @static
   * @returns {Map}
   * @memberof DevourDatabaseModeler
   */
  static getColumnTypeMap () {
    if (!DevourDatabaseModeler.columnTypeMap) {
      DevourDatabaseModeler.columnTypeMap = new Map({
        int: DataTypes.INTEGER,
        str: DataTypes.STRING,
        bigint: DataTypes.BIGINT,
        double: DataTypes.DOUBLE,
        float: DataTypes.FLOAT,
        date: DataTypes.DATE,
        uuid: DataTypes.UUID
      })
    }
    return DevourDatabaseModeler.columnTypeMap
  }

  /**
   *Creates an instance of DevourDatabaseModeler.
   * @param {Sequelize} sequelize Instance of sequelize to use to associate the model to
   * @param {Object} schema Description of the schema of the csv
   * @param {String} [tableName=DEFAULT_TABLE_NAME]
   * @memberof DevourDatabaseModeler
   */
  constructor (sequelize, schema, tableName = DEFAULT_TABLE_NAME) {
    this.sequelize = sequelize
    this.schema = this.buildModel(new Map(schema))
    this.tableName = tableName

    class ImportModel extends Model {}

    debug(
      `Initialize modeler schema for ${this.tableName} with, `,
      this.schema
    )

    ImportModel.init(this.schema.toObject(), {
      sequelize,
      tableName: this.tableName,
      timestamps: false
    })

    this.model = ImportModel
  }

  /**
   * Getter for sequelize model
   *
   * @returns {Model}
   * @memberof DevourDatabaseModeler
   */
  getModel () {
    return this.model
  }

  /**
   * Builds a model schema Sequelize can understand from the given map
   *
   * @param {Object} buildModel Column map
   * @returns {Object}
   * @memberof DevourDatabaseModeler
   */
  buildModel (buildModel) {
    return buildModel.map((columnType) => {
      let databaseColumnTypeParameter

      let databaseColumnType = columnType
      if (databaseColumnType.indexOf('(') > -1) {
        const startInterval = databaseColumnType.indexOf('(')
        const parameterLength = databaseColumnType.length - startInterval - 2
        databaseColumnTypeParameter = databaseColumnType.substr(
          startInterval + 1,
          parameterLength
        )
        databaseColumnType = columnType.substr(0, startInterval)
      }

      const columnTypeMap = DevourDatabaseModeler.getColumnTypeMap()
      const columnDatabaseType = columnTypeMap.get(databaseColumnType)

      if (!columnDatabaseType) {
        throw new Error(`${databaseColumnType} is an unknown column type.`)
      }

      return columnDatabaseType(databaseColumnTypeParameter)
    })
  }
}

module.exports = DevourDatabaseModeler

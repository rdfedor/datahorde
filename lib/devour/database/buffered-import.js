const debug = require('debug')('DataHorde:DevourDatabaseBufferedImportBufferedImport')
const async = require('async')
const { EventEmitter } = require('events')
const DatabaseModeler = require('./modeler')

const { ON_CREATED, ON_FINISHED } = require('../constants')

/**
 * Provides a queued import services to ingest csv
 *
 * @extends {EventEmitter}
 */
class DevourDatabaseBufferedImport extends EventEmitter {
  /**
   * Creates an instance of DatabaseBufferedImport.
   * @param {sequelize} sequelize
   * @memberof DatabaseBufferedImport
   */
  constructor (sequelize) {
    super()

    this.sequelize = sequelize
  }

  /**
   * Initialize Import Queue
   *
   * @param {Object} [schema={}] Description of csv being imported
   * @param {string} [tableName='ImportedTable'] Name of the table to import data into
   * @param {boolean} [forceSync=true] Drops an recreates the table
   * @returns {Promise}
   * @memberof DevourDatabaseBufferedImport
   */
  initialize (schema = {}, tableName = 'ImportedTable', forceSync = false) {
    return new Promise((resolve, reject) => {
      const modeler = new DatabaseModeler(this.sequelize, schema, tableName)

      const queue = async.cargo((tasks, queueCallback) => {
        debug(`BulkCreating ${tasks.length} records.`)

        modeler
          .getModel()
          .bulkCreate(tasks)
          .then(() => {
            queueCallback()
            this.emit(ON_CREATED, tasks.length)
          })
      }, 1000)

      queue.drain(() => {
        this.emit(ON_FINISHED)
      })

      this.sequelize.sync({ force: forceSync }).then(() => {
        resolve(this)
      })

      this.modeler = modeler
      this.queue = queue
      this.schema = schema
      this.tableName = tableName
      this.forceSync = forceSync
    })
  }

  /**
   * Adds a record to the queue for insertion
   *
   * @param {Object} row An object representing a row in the csv
   * @memberof DevourDatabaseBufferedImport
   */
  pushRecord (row) {
    this.queue.push(row)
  }
}

module.exports = DevourDatabaseBufferedImport

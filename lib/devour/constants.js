module.exports = {
  DEFAULT_TABLE_NAME: 'importTable',
  DEFAULT_DATABASE_URI: 'sqlite::memory:',
  DEFAULT_DELIMITER: '|',
  DEFAULT_HAS_HEADERS: true,
  DEFAULT_RENAME_HEADERS: false,
  ON_CREATED: 'created',
  ON_FINISHED: 'finished',
  ON_LOADED: 'loaded',
  ON_LOAD_FINISHED: 'loadFinished'
}

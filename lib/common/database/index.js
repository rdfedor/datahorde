const debug = require('debug')('DataHorde:CommonDatabase')

const Sequelize = require('sequelize')
const EventEmitter = require('events')

const ExtensionArray = require('./extension-array')

const {
  DEFAULT_DATABASE_URI
} = require('../constants')

/**
 * Class for handling inserting data into databases
 *
 * @extends {EventEmitter}
 */
class CommonDatabase extends EventEmitter {
  /**
   *Creates an instance of DevourDatabase.
   * @param {String} databaseUri Sequelize compliant database uri
   * @memberof DevourDatabase
   */
  constructor (databaseUri = DEFAULT_DATABASE_URI) {
    super()

    debug(`Initializing Database against ${databaseUri}`)

    const sequelize = new Sequelize(databaseUri, {
      dialectOptions: {
        multipleStatements: true
      },
      logging: (obj) => debug(JSON.stringify(obj))
    })

    this.databaseUri = databaseUri
    this.sequelize = sequelize
    this.extensions = new ExtensionArray()
  }

  /**
   * Makes a query agaist a database
   *
   * @returns {Sequelize.query}
   * @memberof CommonDatabase
   */
  query () {
    return this.sequelize.query.apply(this.sequelize, arguments)
  }

  /**
   * Registers an extension with the database service
   *
   * @param {*} Extension
   * @param {*} ...args
   * @returns {Promise|*}
   * @memberof CommonDatabase
   */
  registerExtension (Extension, ...args) {
    if (this.extensions.containsInstanceOf(Extension)) {
      throw new Error(
        `Extension ${Extension.constructor.name} already registered`
      )
    }

    debug(`Registering Database Extension ${Extension.name}`)
    const instance = new Extension(this.sequelize)
    this.extensions.push(instance)

    let initialize = null

    if (typeof instance.initialize === 'function') {
      debug(`Initializing Database Extension ${Extension.name}`)
      initialize = instance.initialize(...args)
    }

    return initialize || instance
  }

  /**
   * Removes an extension from the database
   *
   * @param {*} Extension
   * @returns {Boolean}
   * @memberof CommonDatabase
   */
  unRegisterExtension (Extension) {
    const index = this.extensions.findIndexOf(Extension)

    if (index === -1) {
      throw new Error(`${Extension.name} is not registered.`)
    }

    this.extensions[index] = null

    return !!this.extensions.splice(index, 1)
  }

  /**
   * Retieves instance of extension
   *
   * @param {*} Extension
   * @returns  {*}
   * @memberof CommonDatabase
   */
  findExtensionByInstance (Extension) {
    return this.extensions.findInstanceOf(Extension)
  }

  /**
   * Closes the database connection
   *
   * @memberof DevourDatabase
   */
  close () {
    this.sequelize.close()
  }
}

module.exports = CommonDatabase

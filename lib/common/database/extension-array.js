const ExtensionArray = Array

ExtensionArray.prototype.findInstanceOf = function (obj) {
  let extension = null
  for (let i = 0; i < this.length; i += 1) {
    if (this[i] instanceof obj) extension = this[i]
    break
  }
  return extension
}

ExtensionArray.prototype.findIndexOf = function (obj) {
  for (let i = 0; i < this.length; i += 1) {
    if (this[i] instanceof obj) return i
  }
  return -1
}

ExtensionArray.prototype.containsInstanceOf = function (obj) {
  return !!this.findInstanceOf(obj)
}

module.exports = ExtensionArray

module.exports = {
  DEFAULT_DATABASE_URI: 'sqlite::memory:',
  UMZUG_EVENT_MIGRATING: 'migrating',
  UMZUG_EVENT_MIGRATED: 'migrated',
  UMZUG_EVENT_REVERTING: 'reverting',
  UMZUG_EVENT_REVERTED: 'reverted',
  DIGEST_EVENT_EXECUTING: 'digesting',
  DIGEST_EVENT_EXECUTED: 'digested',
  DIGEST_EVENT_SEEDING: 'seeding',
  DIGEST_EVENT_SEEDED: 'seeded',
  DIGEST_EVENT_EGRESSING: 'egressing',
  DIGEST_EVENT_EGRESSED: 'egressed',
  DIGEST_EVENT_CRAWLING: 'crawling',
  DIGEST_EVENT_CRAWLED: 'crawled'
}

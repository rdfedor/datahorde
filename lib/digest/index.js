const debug = require('debug')('DataHorde:Digest')
const EventEmitter = require('events')
const fs = require('fs')
const Path = require('path')

const CommonDatabase = require('../common/database')
const Crawler = require('../crawler')
const DatabaseMigrations = require('./database/migrations')
const Devour = require('../devour')
const Mimic = require('../mimic')
const { URL_DISCOVERED } = require('../crawler/constants')
const {
  DEFAULT_DATABASE_URI,
  UMZUG_EVENT_MIGRATED,
  UMZUG_EVENT_MIGRATING,
  UMZUG_EVENT_REVERTED,
  UMZUG_EVENT_REVERTING,
  DIGEST_EVENT_CRAWLING,
  DIGEST_EVENT_CRAWLED,
  DIGEST_EVENT_EXECUTED,
  DIGEST_EVENT_EXECUTING,
  DIGEST_EVENT_SEEDED,
  DIGEST_EVENT_SEEDING,
  DIGEST_EVENT_EGRESSING,
  DIGEST_EVENT_EGRESSED
} = require('./constants')

/**
 * A workflow tool for processing digest files
 *
 * @extends {EventEmitter}
 */
class Digest extends EventEmitter {
  /**
   * Creates an instance of Digest.
   * @param {String|CommonDatabase} [databaseUri=DEFAULT_DATABASE_URI] A db uri or instance of CommonDatabase
   * @memberof Digest
   */
  constructor (databaseUri = DEFAULT_DATABASE_URI) {
    super()
    this.database = new CommonDatabase(databaseUri)
  }

  /**
   * Initializes database migrator extension
   *
   * @param {Object} [data={}] Data to use to build the scripts for migrations
   * @returns {this}
   * @memberof Digest
   */
  initializeMigrator () {
    this.migrator = this.database.registerExtension(
      DatabaseMigrations,
      this.basePath,
      this.data
    );

    [
      UMZUG_EVENT_MIGRATED,
      UMZUG_EVENT_MIGRATING,
      UMZUG_EVENT_REVERTED,
      UMZUG_EVENT_REVERTING
    ].forEach((event) => {
      this.migrator.on(event, (...args) => {
        this.emit(event, ...args)
      })
    })

    return this
  }

  /**
   * Loads configuration from file
   *
   * @param {String} [basePath=process.cwd()]
   * @returns {this}
   * @memberof Digest
   */
  loadConfiguration (basePath = process.cwd(), data = {}) {
    const digestPath = Path.resolve(basePath, './digest')
    debug('Importing configuration')
    if (
      !fs.existsSync(`${digestPath}.js`) &&
      !fs.existsSync(`${digestPath}.json`)
    ) {
      throw new Error(
        `Digest package does not exist at ${basePath}. Cannot load ${digestPath}.`
      )
    }

    const config = require(digestPath)

    this.config = typeof config === 'function' ? config(data) : config
    this.basePath = basePath
    this.data = { ...this.config.defaultData || {}, ...data }

    return this
  }

  /**
   * Returns the base path as defined by the configuration
   *
   * @returns {String}
   * @memberof Digest
   */
  getBasePath () {
    return this.basePath
  }

  /**
   * Sets the config for the digest instance
   *
   * @param {Object} config
   * @memberof Digest
   */
  setConfig (config) {
    this.config = config
  }

  /**
   * Returns the current digest configuration
   *
   * @returns {Object}
   * @memberof Digest
   */
  getConfig () {
    return this.config
  }

  /**
   * Sets up the crawler and collects data
   *
   * @returns {Promise}
   * @memberof Digest
   */
  crawlTargets () {
    return new Promise((resolve, reject) => {
      const targets = this.config.crawler || []
      debug(`Found ${targets.length} crawlers`)

      let next

      const processNextCrawler = () => {
        if (targets.length) {
          next = targets.shift()
          this.emit(DIGEST_EVENT_CRAWLING, next.paths)

          const crawler = new Crawler(
            next.rowSelector,
            next.columnSelector,
            next.headers,
            next.pageLinkSelector,
            next.proxy,
            next.threads
          )

          crawler.setProfile(next.profile || 'generic')

          if (next.browser) {
            crawler.toggleBrowserEmulation()
          }

          if (next.file) {
            if (!next.file.path) {
              throw new Error('Path must be defined in url devour.file import')
            }
            crawler.queueUrlFromCsv(
              next.file.path,
              next.file.headers,
              next.file.delimiter,
              next.file.renameHeaders
            )
          } else {
            crawler.queueUrl(next.paths)
          }

          crawler.on(URL_DISCOVERED, (url) => this.emit(DIGEST_EVENT_CRAWLING, url))

          crawler
            .outputToCsv(
              Path.resolve(this.basePath, next.output),
              next.headers,
              next.delimiter
            )
            .then(() => {
              this.emit(DIGEST_EVENT_CRAWLED, next.paths)
              processNextCrawler()
            })
            .catch((err) => {
              throw new Error(err)
            })
        } else {
          resolve()
        }
      }

      processNextCrawler()
    })
  }

  /**
   * Imports csvs to a data source via the devour attribute in the configuration
   *
   * @returns {Promise}
   * @memberof Digest
   */
  loadSeeds () {
    return new Promise((resolve, reject) => {
      const seeds = this.config.devour || []
      debug(`Loading ${seeds.length} seeds`)

      const devour = new Devour(this.database)

      let nextSeed

      const processNextSeed = () => {
        if (seeds.length) {
          nextSeed = seeds.shift()

          this.emit(DIGEST_EVENT_SEEDING, nextSeed.path)

          devour
            .initializeDatabaseImportQueue(
              nextSeed.schema,
              nextSeed.tableName,
              nextSeed.forceSync
            )
            .then(() => {
              debug('Devour DatabaseImportQueue initialized')

              devour
                .loadFromCsvFile(
                  Path.resolve(this.basePath, nextSeed.path),
                  nextSeed.hasHeaders,
                  nextSeed.delimiter,
                  nextSeed.renameHeaders
                )
                .then(() => {
                  this.emit(DIGEST_EVENT_SEEDED, nextSeed.path)

                  debug(
                    `${nextSeed.path} imported, unregistering DatabaseImportQueue extension`
                  )
                  devour.unRegisterDatabaseImportQueue()
                  processNextSeed()
                })
            })
        } else {
          resolve()
        }
      }

      processNextSeed()
    })
  }

  /**
   * Mimic's each path and executes against the database
   *
   * @returns {Promise}
   * @memberof Digest
   */
  processPaths () {
    return new Promise((resolve, reject) => {
      const paths = this.config.paths || []
      debug(`Found ${paths.length} paths`)

      let nextPath
      const processNextPath = () => {
        if (paths.length) {
          nextPath = paths.shift()

          this.emit(DIGEST_EVENT_EXECUTING, nextPath)
          this.database
            .query(
              Mimic.generateFromFile(
                Path.resolve(this.basePath, nextPath),
                this.data
              ),
              {
                replacements: this.data
              }
            )
            .then(() => {
              this.emit(DIGEST_EVENT_EXECUTED, nextPath)

              processNextPath()
            })
            .catch((err) => {
              reject(err)
            })
        } else {
          resolve()
        }
      }

      processNextPath()
    })
  }

  /**
   * Exports data from the data source
   *
   * @returns {Promise}
   * @memberof Digest
   */
  processEgressions () {
    return new Promise((resolve, reject) => {
      const egressions = this.config.egressions || []
      debug(`Found ${egressions.length} egressions`)
      const devour = new Devour(this.database)

      let next

      const processNextEggression = () => {
        if (egressions.length) {
          next = egressions.shift()
          this.emit(DIGEST_EVENT_EGRESSING, next.path || 'query')

          let { query } = next

          if (!query && next.path) {
            query = Mimic.generateFromFile(
              Path.resolve(this.basePath, next.path),
              this.data
            )
          } else {
            query = Mimic.generate(query, this.data)
          }

          devour
            .egressToCsv(
              Path.resolve(this.basePath, next.outputPath),
              query,
              next.delimiter,
              next.headers
            )
            .then(() => {
              this.emit(DIGEST_EVENT_EGRESSED, next.path || 'query')
              processNextEggression()
            })
        } else {
          resolve()
        }
      }

      processNextEggression()
    })
  }

  /**
   * Upgrades schema of database
   * @param {Object} options
   * @returns {Promise}
   * @memberof Migrations
   */
  migrateUp () {
    return this.migrator.up.apply(this.migrator, arguments)
  }

  /**
   * Downgrades schema of database
   * @param {Object} options
   * @returns {Promise}
   * @memberof Migrations
   */
  migrateDown () {
    return this.migrator.down.apply(this.migrator, arguments)
  }
}

module.exports = Digest

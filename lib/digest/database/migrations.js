const debug = require('debug')('DataHorde:DatabaseMigrations')
const { EventEmitter } = require('events')
const Path = require('path')
const Umzug = require('umzug')

const {
  UMZUG_EVENT_MIGRATED,
  UMZUG_EVENT_MIGRATING,
  UMZUG_EVENT_REVERTED,
  UMZUG_EVENT_REVERTING
} = require('../constants')

/**
 * Handles migrating database schemas
 *
 * @extends {EventEmitter}
 */
class DigestDatabaseMigrations extends EventEmitter {
  /**
   * Creates an instance of Migrations.
   * @param {require('sequelize').sequelize} sequelize
   * @memberof DigestDatabaseMigrations
   */
  constructor (sequelize) {
    debug('Instantiating Database Migrations')

    super()

    this.sequelize = sequelize
  }

  /**
   * Initializes the Umzug migration service
   *
   * @param {String} [basePath=process.cwd()] Base path to refernce data from
   * @param {Object} [data={}] Parameters to run against the migrations
   * @memberof DigestDatabaseMigrations
   */
  initialize (basePath = process.cwd(), data = {}) {
    debug('Intializing Database Migrations')

    this.migrator = new Umzug({
      storage: 'sequelize',
      storageOptions: {
        sequelize: this.sequelize,
        modelName: 'migration',
        tableName: 'migrations'
      },
      migrations: {
        params: [this.sequelize.getQueryInterface(), this.sequelize.constructor, data],
        path: Path.resolve(basePath, './migrations'),
        pattern: /\.js$/
      }
    });

    [
      UMZUG_EVENT_MIGRATED,
      UMZUG_EVENT_MIGRATING,
      UMZUG_EVENT_REVERTED,
      UMZUG_EVENT_REVERTING
    ].forEach((event) => this.remitMigratorEvent(event))

    this.basePath = basePath
    this.data = data

    return this
  }

  /**
   * Re-emits a given event from migrator
   *
   * @param {String} event Event to re-emit
   * @memberof DigestDatabaseMigrations
   */
  remitMigratorEvent (event) {
    debug(`Remitting ${event}`)
    this.migrator.on(event, (...args) => {
      this.emit(event, ...args)
    })
  }

  /**
   * Upgrades schema of database
   * @param {Object} options
   * @returns {Promise}
   * @memberof DigestDatabaseMigrations
   */
  down () {
    debug('Rolling back migrations')

    return this.migrator.down.apply(this.migrator, arguments)
  }

  /**
   * Downgrades schema of database
   * @param {Object} options
   * @returns {Promise}
   * @memberof DigestDatabaseMigrations
   */
  up () {
    debug('Running migrations')
    return this.migrator.up.apply(this.migrator, arguments)
  }
}

module.exports = DigestDatabaseMigrations

const debug = require('debug')('DataHorde:Mimic')
const ejs = require('ejs')
const fs = require('fs')

/**
 * An interface for templatizing scripts
 *
 * @class Mimic
 */
class Mimic {
  /**
   * Generates template from given template string
   *
   * @static
   * @param {String} template Ejs template
   * @param {Object} data Payload of data to run against the ejs template
   * @param {Object} [options=null] Options passed to ejs
   * @returns {String} Processed ejs template
   * @memberof Mimic
   */
  static generate (template, data, options = null) {
    debug(`Rendering data (${JSON.stringify(data)}) against template`, {
      template
    })
    return ejs.render(template, data, options)
  }

  /**
   * Generates template from local file
   *
   * @static
   * @param {String} file Path to local file to convert
   * @param {Object} data Payload of data to run against template
   * @param {Object} [options=null] Options passed to ejs
   * @returns {String} Processed ejs template
   * @memberof Mimic
   */
  static generateFromFile (file, data, options = null) {
    debug(`Reading the template from ${file}`)
    const template = fs.readFileSync(file, {
      encoding: 'utf8'
    })

    return Mimic.generate(template, data, options)
  }
}

module.exports = Mimic

const { expect } = require('chai')

const CommonDatabase = require('../lib/common/database')
const DatabaseBufferedImport = require('../lib/devour/database/buffered-import')

describe('CommonDatabase', function () {
  describe('can register devour/import-queue', function () {
    let database = null

    it('can instantiate a new database', function () {
      database = new CommonDatabase()
      expect(database instanceof CommonDatabase).to.equal(true)
    })

    it('registers new extension with initialize', function () {
      const queueInstance = database.registerExtension(DatabaseBufferedImport)

      expect(queueInstance instanceof Promise).to.equal(true)
    })

    it('should fail attempting to register it twice', function (done) {
      try {
        database.registerExtension(DatabaseBufferedImport)
      } catch (err) {
        expect(err.message).to.contain('already registered')
        done()
      }
    })

    it('can register an extension after unregistering', function () {
      expect(
        database.findExtensionByInstance(DatabaseBufferedImport)
      ).not.to.equal(null)

      database.unRegisterExtension(DatabaseBufferedImport)

      expect(database.findExtensionByInstance(DatabaseBufferedImport)).to.equal(
        null
      )
    })

    it("should fail attempting to unregister an extension that isn't registered", function (done) {
      try {
        database.unRegisterExtension(DatabaseBufferedImport)
      } catch (err) {
        expect(err.message).to.contain(
          ' is not registered.'
        )
        done()
      }
    })

    it('registers new extension w/o initialize', function () {
      class TestClass {}
      const queueInstance = database.registerExtension(TestClass)

      expect(queueInstance instanceof TestClass).to.equal(true)
    })
  })
})

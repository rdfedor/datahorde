const { expect } = require('chai')
const fs = require('fs')
const nock = require('nock')
const path = require('path')

const Crawler = require('../lib/crawler')
const {
  DEFAULT_PROFILE,
  ON_DATA,
  ON_FINISHED,
  ON_ERROR
} = require('../lib/crawler/constants')

const mockListOfWordsHtml = fs.readFileSync(
  './mock/list-of-words.html',
  'utf8'
)

describe('Crawler', function () {
  describe('returns profiles', function () {
    const crawlerProfiles = Crawler.getProfiles()

    it('should contain generic profile', function () {
      expect(crawlerProfiles.join(',')).to.equal('generic')
    })
  })

  describe('scrape data from mock page', function () {
    let crawler = null

    nock('http://example.com')
      .get('/test')
      .reply(200, mockListOfWordsHtml)

    afterEach(function () {
      crawler.removeAllListeners(ON_DATA)
      crawler.removeAllListeners(ON_ERROR)
      crawler.removeAllListeners(ON_FINISHED)
    })

    it('instantiates a new crawler', function () {
      const { rowSelector, columnSelector } = {
        rowSelector: 'li',
        columnSelector: ['a:text', 'a:attr(href)', 'span']
      }
      crawler = new Crawler(rowSelector, columnSelector)

      crawler.setThreads(2)

      expect(crawler).not.to.equal(null)
    })

    it('successfully set profile to default', function () {
      crawler.setProfile(DEFAULT_PROFILE)

      expect(crawler.getProfile()).to.equal(DEFAULT_PROFILE)
    })

    it('errors when attempting to change to a profile that does not exist', function (done) {
      try {
        crawler.setProfile('failprofile')
      } catch (err) {
        expect(err).not.to.equal(null)
        expect(err.message).to.equal('Profile not supported')
        done()
      }
    })

    it('should not add an empty url', function () {
      expect(crawler.hasUrlQueue()).to.equal(false)

      crawler.queueUrl('')

      expect(crawler.hasUrlQueue()).to.equal(false)
    })

    it('add url to queue', function () {
      expect(crawler.hasUrlQueue()).to.equal(false)

      crawler.queueUrl('http://example.com/test')

      expect(crawler.hasUrlQueue()).to.equal(true)
    })

    it('starts, parses document and calls finish', function (done) {
      let rowData = false
      let crawlerError = false

      crawler.on(ON_DATA, (row) => (rowData = row))
      crawler.on(ON_ERROR, (err) => (crawlerError = err))

      crawler.on(ON_FINISHED, () => {
        expect(crawlerError).to.equal(false)
        expect(rowData.length).to.equal(4)

        done()
      })

      crawler.begin()
    })

    it('outputs detais of scrape to csv', function (done) {
      const outputFile = path.resolve(__dirname, './test.csv')
      crawler.queueUrl('http://example.com/test')

      crawler.toggleBrowserEmulation()

      crawler.outputToCsv(outputFile).then(() => {
        if (fs.existsSync(outputFile)) {
          fs.unlinkSync(outputFile)
          done()
        } else {
          throw new Error(`Unable to locate ${outputFile}`)
        }
      })
    })

    it('can add multiple urls to queue', function () {
      expect(crawler.hasUrlQueue()).to.equal(false)

      crawler.queueUrl(['http://example.com/test', 'http://example.com/test'])

      expect(crawler.hasUrlQueue()).to.equal(true)
    })
  })

  describe('can queue urls from csv', function () {
    const inputCsvPath = path.resolve(__dirname, '../mock/scraper-url.csv')
    const crawler = new Crawler()

    it('should read from a csv file', function (done) {
      crawler.queueUrlFromCsv(inputCsvPath).then((count) => {
        expect(count).to.equal(1)
        done()
      })
    })
  })
})

const { expect } = require('chai')
const path = require('path')

const Mimic = require('../lib/mimic')

describe('Mimic', function () {
  describe('static calls', function () {
    it('should read and render from file', function () {
      expect(
        Mimic.generateFromFile(
          path.resolve(__dirname, '../mock/echo.ejs'),
          {
            name: 'World'
          }
        ).trim()
      ).to.equal('Hello World')
    })
  })
})

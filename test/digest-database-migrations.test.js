const Sequelize = require('sequelize')
const DigestDatabaseMigrations = require('../lib/digest/database/migrations')

describe('DigestDatabaseMigrations', function () {
  describe('should initialize with no parameters', function () {
    const sequalize = new Sequelize('sqlite::memory:')
    let databaseMigration
    it('should read and render from file', function (done) {
      databaseMigration = new DigestDatabaseMigrations(sequalize)

      databaseMigration.initialize()

      done()
    })
  })
})

const { expect } = require('chai')
const sinon = require('sinon')
const fs = require('fs')
const path = require('path')

const Devour = require('../lib/devour')

const { ON_CREATED } = require('../lib/devour/constants')

describe('Devour', function () {
  describe('has static functions which should', function () {
    it('return the available schema types', function () {
      expect(Devour.getSchemaColumnTypes()).to.deep.equal([
        'int',
        'str',
        'bigint',
        'double',
        'float',
        'date',
        'uuid'
      ])
    })
  })

  describe('imports pipe delimited csv with headers', function () {
    const schema = { title: 'str(100)', url: 'str' }
    const invalidSchema = { title: 'str(100)', url: 'no-type' }
    const inputFilePath = path.resolve(__dirname, '../mock/data-headers.csv')
    let devour = new Devour()

    it('should error when initializing an unknown schema type', function (done) {
      devour
        .initializeDatabaseImportQueue(invalidSchema)
        .then(() => {
          throw new Error('Failed to error on invalid schema')
        })
        .catch((err) => {
          expect(err.message.indexOf('is an unknown column type.')).to.above(
            -1
          )
          done()
        })
    })

    it('should instantiate a devour instance', function (done) {
      devour = new Devour()

      devour.initializeDatabaseImportQueue(schema).then(() => {
        expect(devour).not.to.equal(null)
        done()
      })
    })

    it('should add 4 records and finish', function (done) {
      let createCount = 0
      devour.on(ON_CREATED, (rows) => (createCount += rows))

      devour
        .loadFromCsvFile(inputFilePath)
        .then(() => {
          devour.close()

          expect(createCount).to.equal(4)

          done()
        })
        .catch((err) => console.error(err))
    })
  })

  describe('imports pipe delimited csv with no headers', function () {
    const schema = { title: 'str', url: 'str' }
    const tableName = 'ImportedData'
    const sqlitePath = path.resolve(__dirname, 'devour-test.sqlite')

    let devour = null

    it('should instantiate a devour instance', function (done) {
      devour = new Devour(`sqlite:${sqlitePath}`)

      devour.initializeDatabaseImportQueue(schema, tableName).then(() => {
        expect(devour).not.to.equal(null)
        done()
      })
    })

    it('should add 4 records and finish', function (done) {
      let createCount = 0
      devour.on(ON_CREATED, (rows) => (createCount += rows))

      devour
        .loadFromCsvFile(
          path.resolve(__dirname, '../mock/data-noheaders.csv'),
          false
        )
        .then(() => {
          expect(createCount).to.equal(4)

          done()
        })
        .catch((err) => console.error(err))
    })

    it('should egress data without erroring', function (done) {
      const outputFile = path.resolve(__dirname, './devour-test.csv')
      const catchSpy = sinon.spy(console, 'error')

      devour
        .egressToCsv(outputFile, 'select * from ImportedData')
        .catch((err) => console.error(err))
        .finally(() => {
          fs.unlinkSync(outputFile)
          fs.unlinkSync(sqlitePath)

          expect(catchSpy.called).to.equal(false)
          done()
        })
    })
  })
})

const { expect } = require('chai')
const path = require('path')
const nock = require('nock')
const fs = require('fs')
const Digest = require('../lib/digest')

const {
  DIGEST_EVENT_SEEDED,
  DIGEST_EVENT_SEEDING,
  DIGEST_EVENT_EXECUTED,
  DIGEST_EVENT_EXECUTING,
  DIGEST_EVENT_EGRESSED,
  DIGEST_EVENT_EGRESSING,
  UMZUG_EVENT_MIGRATED,
  UMZUG_EVENT_MIGRATING,
  UMZUG_EVENT_REVERTED,
  UMZUG_EVENT_REVERTING
} = require('../lib/digest/constants')

describe('Digest', function () {
  describe('output a report for the digest-example', function () {
    let digest = null
    const outputPath = path.resolve(__dirname, './test-output.csv')
    const mockListOfWordsHtml = fs.readFileSync(
      './mock/list-of-words.html',
      'utf8'
    )

    nock('http://example.com')
      .get('/test')
      .reply(200, mockListOfWordsHtml)

    it('should instantiate a digest instance', function () {
      digest = new Digest()

      expect(digest).not.to.equal(null)
    })

    it('should be able to setConfig for crawling a url', function () {
      digest.setConfig({
        crawler: [
          {
            paths: 'http://example.com/test',
            rowSelector: 'ul',
            columnSelector: ['li'],
            headers: ['word'],
            output: outputPath
          }
        ]
      })
    })

    it('should export data to csv from a url', function (done) {
      digest
        .crawlTargets()
        .then(() => {
          if (!fs.existsSync(outputPath)) {
            throw new Error(`${outputPath} not found`)
          }
          fs.unlinkSync(outputPath)
          done()
        })
        .catch((err) => {
          throw new Error(err)
        })
    })

    const mockInputCsv = path.resolve(__dirname, '../mock/scraper-url.csv')

    it('should be able to setConfig for crawling urls in a file', function () {
      digest.setConfig({
        crawler: [
          {
            file: {
              path: mockInputCsv
            },
            browser: true,
            rowSelector: 'ul',
            columnSelector: 'li',
            headers: ['word'],
            output: outputPath
          }
        ]
      })

      expect(digest.getConfig().crawler[0].browser).to.equal(true)
    })

    it('should export data to csv from urls from a csv', function (done) {
      digest
        .crawlTargets()
        .then(() => {
          if (!fs.existsSync(outputPath)) {
            throw new Error(`${outputPath} not found`)
          }
          fs.unlinkSync(outputPath)
          done()
        })
        .catch((err) => {
          throw new Error(err)
        })
    })

    it('should error loading a missing configuration', function (done) {
      try {
        digest.loadConfiguration()
      } catch (err) {
        expect(err.message).to.contain('Digest package does not exist')
        done()
      }
    })

    it('should load the configuration', function () {
      digest.loadConfiguration(
        path.resolve(__dirname, '../examples/digest-basic-example')
      )
      expect(digest.getBasePath()).to.contain('examples/digest-basic-example')
    })

    it('should initialize the migrator without erroring', function () {
      digest.initializeMigrator()
    })

    it('should process files in the migrations folder', function (done) {
      let migrated = 0
      let migrating = 0
      digest.on(UMZUG_EVENT_MIGRATING, () => {
        migrating += 1
      })
      digest.on(UMZUG_EVENT_MIGRATED, () => {
        migrated += 1
      })
      digest.migrateUp().then(() => {
        expect(migrating).to.equal(1)
        expect(migrated).to.equal(1)
        done()
      })
    })

    it('should rollback migrations', function (done) {
      let reverted = 0
      let reverting = 0
      digest.on(UMZUG_EVENT_REVERTING, () => {
        reverting += 1
      })
      digest.on(UMZUG_EVENT_REVERTED, () => {
        reverted += 1
      })
      digest.migrateDown().then(() => {
        expect(reverting).to.equal(1)
        expect(reverted).to.equal(1)
        digest.migrateUp().then(() => {
          done()
        })
      })
    })

    it('should process seeds through devour', function (done) {
      let seeding = 0
      let seeded = 0
      digest.on(DIGEST_EVENT_SEEDING, () => {
        seeding += 1
      })
      digest.on(DIGEST_EVENT_SEEDED, () => {
        seeded += 1
      })
      digest.loadSeeds().then(() => {
        expect(seeding).to.equal(1)
        expect(seeded).to.equal(1)
        done()
      })
    })

    it('should execute path files against the data source', function (done) {
      let executing = 0
      let executed = 0
      digest.on(DIGEST_EVENT_EXECUTING, () => {
        executing += 1
      })
      digest.on(DIGEST_EVENT_EXECUTED, () => {
        executed += 1
      })
      digest.processPaths().then(() => {
        expect(executing).to.equal(1)
        expect(executed).to.equal(1)
        done()
      })
    })

    it('should run available egressions', function (done) {
      let egressing = 0
      let egressed = 0
      digest.on(DIGEST_EVENT_EGRESSING, () => {
        egressing += 1
      })
      digest.on(DIGEST_EVENT_EGRESSED, () => {
        egressed += 1
      })
      digest.processEgressions().then(() => {
        expect(egressing).to.equal(1)
        expect(egressed).to.equal(1)
        done()
      })
    })
  })
})

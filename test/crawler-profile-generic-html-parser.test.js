const { expect } = require('chai')
const fs = require('fs')
const GenericHtmlParser = require('../lib/crawler/profile/generic/html-parser')

const mockListOfWordsHtml = fs.readFileSync('./mock/list-of-words.html', 'utf8')

describe('GenericHtmlParser', function () {
  describe('Parses documents', function () {
    const { rowSelector, columnSelector, nextPageLinkSelector } = {
      rowSelector: 'li',
      columnSelector: ['a:text', 'a:attr(href)', 'span'],
      nextPageLinkSelector: '.nextPage:attr(href)'
    }
    const parser = new GenericHtmlParser(
      rowSelector,
      columnSelector,
      null,
      nextPageLinkSelector
    )

    it('should parse a document', function () {
      const doc = parser.parseDocument(mockListOfWordsHtml)

      expect(doc.nextPageLink).to.equal('#link5')
      expect(doc.rows.length).to.equal(4)
      expect(doc.rows[0][0]).to.equal('Marvel')
      expect(doc.rows[0][1]).to.equal('#link1')
      expect(doc.rows[0][2]).to.equal('')
    })

    it('should return no rows with an empty document', function () {
      const doc = parser.parseDocument('')
      expect(doc.rows.length).to.equal(0)
    })
  })
})

const { expect } = require('chai')
const fs = require('fs')
const nock = require('nock')

const GenericProfileCrawler = require('../lib/crawler/profile/generic')
const {
  ON_DATA,
  ON_FINISHED,
  ON_URL_DISCOVERED,
  ON_ERROR
} = require('../lib/crawler/constants')

const mockListOfWordsHtml = fs.readFileSync(
  './mock/list-of-words.html',
  'utf8'
)

describe('GenericProfileCrawler', function () {
  describe('mock crawl http://example.com/test', function () {
    beforeEach(function () {
      nock('http://example.com')
        .get('/test')
        .reply(200, mockListOfWordsHtml)
    })
    it('should error without a url', function () {
      let crawlerError = null
      try {
        const crawler = new GenericProfileCrawler()
        if (crawler) {
          throw new Error('GeneriProfileCrawler did not error.')
        }
      } catch (err) {
        crawlerError = err
      }

      expect(crawlerError).not.to.equal(null)
      expect(crawlerError.message).to.contain('url')
    })

    it('should error with an invalid column selector', function (done) {
      const { url, options } = {
        url: 'http://example.com/test',
        options: {
          rowSelector: 'li',
          columnSelector: ['a:failTest']
        }
      }
      const crawler = new GenericProfileCrawler(url, options)

      let crawlerError = null

      crawler.on(ON_ERROR, err => {
        crawlerError = err
      })

      crawler.on(ON_FINISHED, () => {
        expect(crawlerError).not.to.equal(null)
        expect(crawlerError.message).to.equal(
          'failTest is an unknown column selector function'
        )
        done()
      })

      crawler.start()
    })

    it('should scrape row data', function (done) {
      const { url, options } = {
        url: 'http://example.com/test',
        options: {
          rowSelector: 'li',
          columnSelector: ['a:text', 'a:attr(href)', 'span'],
          nextPageLinkSelector: '.nextPage:attr(href)'
        }
      }
      const crawler = new GenericProfileCrawler(url, options)

      let nextLink = null
      let crawlerData = null
      let crawlerError = null

      crawler.on(ON_ERROR, err => {
        crawlerError = err
        console.error(err)
      })

      crawler.on(ON_URL_DISCOVERED, url => {
        nextLink = url
      })

      crawler.on(ON_DATA, rows => {
        crawlerData = rows
      })

      crawler.on(ON_FINISHED, () => {
        expect(crawlerError).to.equal(null)
        expect(nextLink).to.equal('http://example.com/test#link5')
        expect(crawlerData.length).to.equal(4)
        done()
      })

      crawler.start()
    })
  })
})

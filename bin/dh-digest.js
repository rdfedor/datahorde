#!/usr/bin/env node

const debug = require('debug')('DataHorde:Mimic-CLI')
const Path = require('path')

const Digest = require('../lib/digest')
const { parseDataString } = require('./util')
const packageJson = require('../package')

const {
  DEFAULT_DATABASE_URI,
  UMZUG_EVENT_MIGRATED,
  UMZUG_EVENT_MIGRATING,
  UMZUG_EVENT_REVERTED,
  UMZUG_EVENT_REVERTING,
  DIGEST_EVENT_EXECUTED,
  DIGEST_EVENT_EXECUTING,
  DIGEST_EVENT_SEEDED,
  DIGEST_EVENT_SEEDING,
  DIGEST_EVENT_EGRESSING,
  DIGEST_EVENT_EGRESSED,
  DIGEST_EVENT_CRAWLING,
  DIGEST_EVENT_CRAWLED
} = require('../lib/digest/constants')

module.exports = commander =>
  commander
    .command('digest <action> <basePath>')
    .alias('di')
    .version(packageJson.version)
    .description(
      'Processes digest action [execute,crawl,seed,egress,migrate,rollback] against a particular database'
    )
    .option(
      '-d, --data <name>=<value>,[<otherData>...]',
      'digest data used when generating migrations and sql'
    )
    .option(
      '-u, --databaseUri <uri>',
      'DatabaseUri supported by Sequelize library. Mysql, MariaDB, MSSQL, SQLite, PSQL supported.',
      DEFAULT_DATABASE_URI
    )
    .action((action, basePath, cmdObj) => {
      console.log(
        `DataHorde-CLI Digest - v${packageJson.version} - ${packageJson.author}`
      )

      const data = parseDataString(cmdObj.data || '')

      debug(`Mapped data to ${JSON.stringify(data)}`)

      try {
        const digest = new Digest(cmdObj.databaseUri)
          .loadConfiguration(Path.resolve(process.cwd(), basePath), data)
          .initializeMigrator();

        [
          UMZUG_EVENT_MIGRATED,
          UMZUG_EVENT_MIGRATING,
          UMZUG_EVENT_REVERTED,
          UMZUG_EVENT_REVERTING,
          DIGEST_EVENT_EXECUTED,
          DIGEST_EVENT_EXECUTING,
          DIGEST_EVENT_SEEDED,
          DIGEST_EVENT_SEEDING,
          DIGEST_EVENT_EGRESSING,
          DIGEST_EVENT_EGRESSED,
          DIGEST_EVENT_CRAWLING,
          DIGEST_EVENT_CRAWLED
        ].forEach(event =>
          digest.on(event, file =>
            console.log(
              `${event.charAt(0).toUpperCase() + event.slice(1)} ${file}`
            )
          )
        )

        const PROCESS_ROLLBACK = 1
        const PROCESS_MIGRATE = 2
        const PROCESS_SEED = 3
        const PROCESS_EGRESSION = 4
        const PROCESS_EXECUTE = 5
        const PROCESS_CRAWL = 6
        const digestConfig = digest.getConfig()
        let command = new Promise(resolve => {
          resolve()
        })

        const processNextStep = (step, remainingSteps = []) => {
          switch (step) {
            case PROCESS_ROLLBACK:
              console.log('Rolling back...')
              command = digest.migrateDown().then(() => {
                console.log('Rollback Completed')
              })
              break
            case PROCESS_MIGRATE:
              console.log('Running Migations...')
              command = digest.migrateUp().then((...args) => {
                console.log('Migration Completed')
              })
              break
            case PROCESS_CRAWL:
              if (digestConfig.crawler) {
                console.log('Crawling Paths...')
                command = digest.crawlTargets().then(() => {
                  console.log('Paths Crawled')
                })
              }
              break
            case PROCESS_SEED:
              if (digestConfig.devour) {
                console.log('Devouring Seeds...')
                command = digest.loadSeeds().then(() => {
                  console.log('Seeds Devoured')
                })
              }
              break
            case PROCESS_EGRESSION:
              if (digestConfig.egressions) {
                console.log('Writing Egressions...')
                command = digest.processEgressions().then(() => {
                  console.log('Egressions Written')
                })
              }
              break
            case PROCESS_EXECUTE:
              if (digestConfig.paths) {
                console.log('Digesting Package...')
                command = digest.processPaths().then(() => {
                  console.log('Package Digested')
                })
              }
              break
            default:
              throw new Error(`${action} is an unknown action.`)
          }

          if (remainingSteps.length > 0) {
            command.then(() =>
              processNextStep(remainingSteps.shift(), remainingSteps)
            )
          }

          return command.catch(err => {
            debug(err)
            console.error(err.message)
          })
        }

        let rootCommand

        if (/^ro/i.test(action)) {
          rootCommand = processNextStep(PROCESS_ROLLBACK)
        } else if (/^mi/i.test(action)) {
          rootCommand = processNextStep(PROCESS_MIGRATE)
        } else if (/^se/i.test(action)) {
          rootCommand = processNextStep(PROCESS_SEED)
        } else if (/^cr/i.test(action)) {
          rootCommand = processNextStep(PROCESS_CRAWL)
        } else if (/^eg/i.test(action)) {
          rootCommand = processNextStep(PROCESS_EGRESSION)
        } else if (/^ex/i.test(action)) {
          rootCommand = processNextStep(PROCESS_CRAWL, [
            PROCESS_MIGRATE,
            PROCESS_SEED,
            PROCESS_EXECUTE,
            PROCESS_EGRESSION
          ])
        } else {
          throw new Error(`${action} is an unknown action.`)
        }

        rootCommand.catch(err => {
          debug(err)
          console.error(err.message)
        })
      } catch (err) {
        debug(err)
        console.error(err.message)
        process.exit(1)
      }
    })

#!/usr/bin/env node

const commander = require('commander')

const commands = ['crawl', 'devour', 'digest', 'mimic', 'egress']

commands.forEach((command) => require(`./dh-${command}.js`)(commander))

commander.parse(process.argv)

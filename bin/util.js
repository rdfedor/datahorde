module.exports = {
  parseDataString: (string) => {
    const splitDataArray = string.split(',')
    const data = {}
    let dataName; let
      dataValue

    splitDataArray.forEach((dataNameValue) => {
      [dataName, dataValue] = dataNameValue.split('=')

      if (data[dataName]) {
        if (Array.isArray(data[dataName])) {
          data[dataName].push(dataValue)
        } else {
          data[dataName] = [data[dataName], dataValue]
        }
      } else {
        data[dataName] = dataValue
      }
    })

    return data
  }
}

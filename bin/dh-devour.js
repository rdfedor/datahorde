#!/usr/bin/env node

const debug = require('debug')('DataHorde:Devour-CLI')
const path = require('path')

const Devour = require('../lib/devour')
const packageJson = require('../package')
const { parseDataString } = require('./util')

const {
  DEFAULT_TABLE_NAME,
  DEFAULT_DATABASE_URI,
  DEFAULT_DELIMITER,
  ON_CREATED,
  ON_FINISHED
} = require('../lib/devour/constants')

module.exports = commander =>
  commander
    .command('devour <inputFile>')
    .alias('d')
    .version(packageJson.version)
    .description('Allows for the easy ingestion of csvs to various datasources')
    .option(
      '-s, --schema <name>=<columnType>,[<otherColumns>...]',
      `describe the csv file and the import table model. (Types: ${Devour.getSchemaColumnTypes().join(
        ', '
      )})`
    )
    .option(
      '-d, --delimiter <delimiter>',
      'Delimiter separating columns in csv.',
      DEFAULT_DELIMITER
    )
    .option(
      '-u, --databaseUri <uri>',
      'DatabaseUri supported by Sequelize library. Mysql, MariaDB, MSSQL, SQLite, PSQL supported.',
      DEFAULT_DATABASE_URI
    )
    .option(
      '-t, --tableName <tableName>',
      'Name of the table to create at the databaseUri.',
      DEFAULT_TABLE_NAME
    )
    .option('-f, --forceSync', 'Forces the re-creation of the target table')
    .option('-h, --noHeaders', 'Csv file has no headers')
    .option('-r, --renameHeaders', 'Rename headers to match schema on import')
    .action((inputFile, cmdObj) => {
      console.log(
        `DataHorde-CLI Devour - v${packageJson.version} - ${packageJson.author}`
      )

      const inputPath = path.resolve(process.cwd(), inputFile)

      const schema = parseDataString(cmdObj.schema)

      debug(`Mapped schema to ${JSON.stringify(schema)}`)

      let devour = null
      let createCount = 0

      try {
        debug(
          `Setting up database connection, ${JSON.stringify({
            schema,
            databaseUri: cmdObj.databaseUri,
            tableName: cmdObj.tableName
          })}`
        )
        devour = new Devour(cmdObj.databaseUri)
      } catch (err) {
        console.error(err)
        process.exit(1)
      }

      devour.on(ON_CREATED, rows => (createCount += rows))

      devour.on(ON_FINISHED, () => {})

      debug(`Loading file ${inputPath} wit delimiter ${cmdObj.delimiter}`)
      devour
        .initializeDatabaseImportQueue(
          schema,
          cmdObj.tableName,
          !!cmdObj.forceSync
        )
        .then(() => {
          devour
            .loadFromCsvFile(
              inputPath,
              !cmdObj.noHeaders,
              cmdObj.delimiter,
              cmdObj.renameHeaders
            )
            .then(count => {
              debug(`Queued ${count} to be inserted`)
              console.log(
                `Import finished. Imported ${createCount} records in ${cmdObj.tableName}.`
              )
              devour.close()
            })
            .catch(err => {
              console.error(err)
              process.exit(1)
            })
        })
        .catch(err => {
          debug(err)
          console.error(err.message)
          process.exit(1)
        })
    })

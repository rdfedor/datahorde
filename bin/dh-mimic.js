#!/usr/bin/env node

const debug = require('debug')('DataHorde:Mimic-CLI')
const fs = require('fs')
const path = require('path')

const Mimic = require('../lib/mimic')
const packageJson = require('../package')

module.exports = commander =>
  commander
    .command('mimic <templateFile> <outputFile>')
    .alias('m')
    .version(packageJson.version)
    .description('Templating engine which uses ejs to build customized scripts')
    .option(
      '-d, --data <name>=<value>,[<otherData>...]',
      'generates template using the given values'
    )
    .option(
      '-n, --noOverwrite',
      'does not overwrite the file if the outputFile already exists'
    )
    .action((templateFile, outputFile, cmdObj) => {
      console.log(
        `DataHorde-CLI Mimic - v${packageJson.version} - ${packageJson.author}`
      )

      const outputPath = path.resolve(process.cwd(), outputFile)
      const splitDataArray = cmdObj.data.split(',')
      const data = {}
      let dataName, dataValue

      splitDataArray.forEach(dataNameValue => {
        [dataName, dataValue] = dataNameValue.split('=')

        if (data[dataName]) {
          if (Array.isArray(data[dataName])) {
            data[dataName].push(dataValue)
          } else {
            data[dataName] = [data[dataName], dataValue]
          }
        } else {
          data[dataName] = dataValue
        }
      })

      debug(`Mapped data to ${JSON.stringify(data)}`)

      try {
        const output = Mimic.generateFromFile({
          file: path.resolve(process.cwd(), templateFile),
          data
        })

        debug('Template render resulted in,', output)

        let flag = 'w'

        if (cmdObj.noOverwrite) {
          flag = 'wx'
        }

        debug(`Writing template render to ${outputPath}`)

        fs.writeFileSync(outputPath, output, {
          flag
        })

        console.log('File rendered successfully.')
      } catch (err) {
        debug(err)
        console.error(err.message)
        process.exit(1)
      }
    })

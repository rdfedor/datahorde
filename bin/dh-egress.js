#!/usr/bin/env node

const debug = require('debug')('DataHorde:Mimic-CLI')
const path = require('path')

const Devour = require('../lib/devour')
const packageJson = require('../package')

const {
  DEFAULT_DATABASE_URI,
  DEFAULT_DELIMITER,
  DEFAULT_HAS_HEADERS
} = require('../lib/devour/constants')

module.exports = commander =>
  commander
    .command('egress <path>')
    .alias('v')
    .version(packageJson.version)
    .description('Exports data from a datasource using a query to csv. Path by default expects a query.')
    .option('-o, --output <filepath>', 'write data to csv')
    .option('-f, --file', 'attempts to read query from file located at path')
    .option(
      '-d, --delimeter <delimeter>',
      'delimeter separating column data',
      DEFAULT_DELIMITER
    )
    .option(
      '-u, --databaseUri <uri>',
      'DatabaseUri supported by Sequelize library. Mysql, MariaDB, MSSQL, SQLite, PSQL supported.',
      DEFAULT_DATABASE_URI
    )
    .option(
      '-h, --headers <headers>[, otherHeaders...]',
      'comma separated list of headers in the csv',
      DEFAULT_HAS_HEADERS
    )
    .action((query, cmdObj) => {
      console.log(
        `DataHorde-CLI Vomit - v${packageJson.version} - ${packageJson.author}`
      )

      const outputPath = path.resolve(process.cwd(), cmdObj.output)
      let headers = cmdObj.headers

      const devour = new Devour(cmdObj.databaseUri)

      if (typeof headers === 'string') {
        headers = headers.split(',')
      }

      devour
        .egressToCsv(outputPath, query, cmdObj.delimeter, headers)
        .then(count => {
          console.log(`Exported ${count} records.`)
        })
        .catch(err => {
          debug(err)
          console.error(err.message)
          process.exit(1)
        })
    })

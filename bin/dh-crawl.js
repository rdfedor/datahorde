#!/usr/bin/env node

const debug = require('debug')('DataHorde:Crawler-CLI')
const stringify = require('json-stringify-safe')
const path = require('path')

const Crawler = require('../lib/crawler')
const {
  ON_DATA,
  ON_FINISHED,
  ON_URL_DISCOVERED,
  DEFAULT_PROFILE,
  DEFAULT_ROW_SELECTOR,
  DEFAULT_COLUMN_SELECTOR,
  DEFAULT_THREADS
} = require('../lib/crawler/constants')
const packageJson = require('../package')

module.exports = commander =>
  commander
    .version(packageJson.version)
    .command('crawl <outputFile> <targetUrl> [otherTargetUrls...]')
    .description(
      'Searches a collection of urls, collects data and writes to csv'
    )
    .alias('c')
    .option('-l, --load', 'load targetUrls from local csv')
    .option(
      '-k, --loadUrlKey <loadUrlKey>',
      'the header that contains the url',
      'url'
    )
    .option(
      '-e, --loadDelimiter <loadDelimiter>',
      'delimeter separating columns in the csv',
      '|'
    )
    .option(
      '-a, --loadHeaders <column1>[,column2...]',
      'header name of columns in the csv',
      true
    )
    .option('-m, --loadRenameHeader', 'map column data to headers', false)
    .option(
      '-p, --profile <name>',
      `use a specific crawler profile (Available: ${Crawler.getProfiles().join(
        ', '
      )})`,
      DEFAULT_PROFILE
    )
    .option(
      '-r, --rowSelector <selector>',
      'css selector to the rows to select the data from',
      DEFAULT_ROW_SELECTOR
    )
    .option(
      '-c, --columnSelector <selector>[,otherSelectors...]',
      'css selector to the values inside the rows',
      DEFAULT_COLUMN_SELECTOR
    )
    .option(
      '-n, --nextPageLinkSelector <selector>',
      'css selector to url for next page'
    )
    .option(
      '-o, --outputHeaders <name>[,otherColumns...]',
      'displays headers at the top of the export'
    )
    .option(
      '-h, --headers <header>=<value>[,otherHeaders...]',
      'set header values in the requests made'
    )
    .option('-x, --proxy <proxy>', 'path to the socks proxy')
    .option(
      '-t, --threads <threads>',
      'run multiple threads at the same time',
      DEFAULT_THREADS
    )
    .option('-b, --browser', 'emulate a browser request')
    .action((outputFile, targetUrl, otherTargetUrls, cmdObj) => {
      console.log(
        `DataHorde-CLI Crawler - v${packageJson.version} - ${packageJson.author}`
      )
      const {
        profile,
        rowSelector,
        columnSelector,
        headers,
        nextPageLinkSelector,
        proxy,
        threads,
        loadRenameHeaders,
        loadDelimeter,
        loadHeaders,
        loadUrlKey
      } = {
        profile: cmdObj.profile,
        rowSelector: cmdObj.rowSelector,
        columnSelector: cmdObj.columnSelector.split(','),
        headers: (cmdObj.headers || '').split(','),
        nextPageLinkSelector: cmdObj.nextPageLinkSelector,
        proxy: cmdObj.proxy,
        threads: cmdObj.threads,
        loadRenameHeaders: cmdObj.loadRenameHeader,
        loadDelimeter: cmdObj.loadDelimeter || '|',
        loadHeaders: cmdObj.loadHeaders,
        loadUrlKey: cmdObj.loadUrlKey
      }

      if (headers && headers.length) {
        headers.forEach(headerString => {
          const [name, value] = headerString.split('=')
          if (name && name.length && value) {
            headers[name] = value
          }
        })
      }

      debug(
        `Triggered crawler-cli with ${stringify({
          profile,
          rowSelector,
          columnSelector,
          headers,
          nextPageLinkSelector,
          proxy,
          threads,
          loadRenameHeaders,
          loadDelimeter,
          loadHeaders,
          loadUrlKey
        })}`
      )

      const crawler = new Crawler(
        rowSelector,
        columnSelector,
        headers,
        nextPageLinkSelector,
        proxy,
        threads
      )

      crawler.setProfile(profile)

      if (cmdObj.browser) {
        crawler.toggleBrowserEmulation()
      }

      let rowCount = 0

      crawler.on(ON_DATA, rows => {
        console.log(`Found ${rows.length} records...`)
        rowCount += rows.length
      })

      crawler.on(ON_FINISHED, () =>
        console.log(`\n\rExport completed capturing ${rowCount} records.`)
      )

      crawler.on(ON_URL_DISCOVERED, () => console.log('Found new page...'))

      console.log('Starting crawler...')

      if (cmdObj.load) {
        const urlFilePaths = [targetUrl].concat(otherTargetUrls)
        Promise.all(
          urlFilePaths.map(csvPath =>
            crawler.queueUrlFromCsv(
              path.resolve(process.cwd(), csvPath.trim()),
              loadHeaders,
              loadDelimeter,
              loadUrlKey,
              loadRenameHeaders
            )
          )
        )
          .then(() => crawler.outputToCsv(outputFile, headers))
          .catch(err => {
            debug(err)
            console.error(err.message)
          })
      } else {
        crawler.queueUrl(targetUrl)
        crawler.queueUrl(otherTargetUrls)
        crawler.outputToCsv(outputFile, headers)
      }
    })

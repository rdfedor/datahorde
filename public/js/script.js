(function (contentEl) {
  const linkMap = [
    'coverage/index.html',
    'jsdoc/index.html'
  ]
  document.setContentHeight = function onSetDocumentContentHeight () {
    contentEl.setAttribute(
      'height',
      `${(window.innerHeight - 40).toString()}px`
    )
  }
  window.onresize = function onResizeWindow (e) {
    document.setContentHeight()
  }
  document.setContentHeight()

  function getJsonFromUrl (url) {
    if (!url) url = location.search
    const query = url.substr(1)
    const result = {}
    query.split('&').forEach((part) => {
      const item = part.split('=')
      result[item[0]] = decodeURIComponent(item[1])
    })
    return result
  }

  const queryParameters = getJsonFromUrl(window.location.search)
  console.log(queryParameters)

  if (linkMap.indexOf(queryParameters.path) > -1) {
    contentEl.setAttribute('src', queryParameters.path)
  }
}(document.getElementById('content')))
